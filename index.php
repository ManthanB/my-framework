<?php
ini_set("display_errors", 1);
error_reporting(E_ALL);

//define("PATH", "http://local.mymvc.com");
require "Library/Constants.php";

function __autoload($className){
	$className = ltrim($className, '\\');
    $fileName  = '';
    $namespace = '';
    if ($lastNsPos = strripos($className, '\\')) {
        $namespace = substr($className, 0, $lastNsPos);
        $className = substr($className, $lastNsPos + 1);
        $fileName  = str_replace('\\', DIRECTORY_SEPARATOR, $namespace) . DIRECTORY_SEPARATOR;
    }
    $fileName .= str_replace('_', DIRECTORY_SEPARATOR, $className) . '.php';
    require $fileName;
}


/*function __autoload($class){
	require 'Library/'.$class.".php";
}*/


$GLOBALS['route'] = new Library\Router();

//$GLOBALS['route']->render("Backend", "index", "index");

?>
