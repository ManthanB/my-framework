<?php
namespace Library;

class Validator{

	function __construct(){

	}

	public function validate($input, $type = 0){

		switch($type){
		 	case 0:
				$result = $this->validateNumbersOnly($input);
				break;

		 	case 1:
				$result = $this->validateAlphaOnly($input);
				break;

		 	case 2:
				$result = $this->validateAlphaNumeric($input);
				break;

		 	case 3:
				$result = $this->validateURL($input);
				break;

			case 4:
				$result = $this->validateEmailId($input);
				break;

		 	default:
				$result = true;
				break;

		}
		return $result;
	}

	public function validateNumbersOnly($input){
		$input = $this->filter($input);
		if(!preg_match("/^[0-9]*$/", $input)){
			return false;
		}else{
			return true;
		}
	}

	public function validateAlphaOnly($input){
		$input = $this->filter($input);
		$input = str_replace(" ","",$input);
		if(!preg_match("/^[a-zA-Z\ ]*$/", $input)){
			return false;
		}else{
			return true;
		}
	}

	public function validateAlphaNumeric($input){
		$input = $this->filter($input);
		$input = str_replace(" ","",$input);
		if(!preg_match("/^[a-zA-Z0-9]*$/", $input)){
			return false;
		}else{
			return true;
		}
	}

	public function validateEmailId($input){
		$input = $this->filter($input);
		if (!filter_var($input, FILTER_VALIDATE_EMAIL)) {
		  return false;
		}else{
			return true;
		}
	}

	public function validateURL($input){
		$input = $this->filter($input);
		if (!preg_match("/\b(?:(?:https?|ftp):\/\/|www\.)[-a-z0-9+&@#\/%?=~_|!:,.;]*[-a-z0-9+&@#\/%=~_|]/i",$input)) {
		  return false;
		}else{
			return true;
		}
	}

	public function filter($input){
		$input = trim($input);
	  $input = stripslashes($input);
	  $input = htmlspecialchars($input);
	  return $input;
	}

}

?>
