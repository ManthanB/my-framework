<?php
namespace Library;

class View{

	function __construct($app, $page){
		$this->app = $app;
		$this->page = $page;
	}

	function render($action, $linkHeaders = 1){
		if($linkHeaders==1){
			if(is_file("Apps/$this->app/Resources/Partials/Header.php")){
				require "Apps/$this->app/Resources/Partials/Header.php";
			}

			require "Apps/$this->app/Resources/Private/".$action.".php";

			if(is_file("Apps/$this->app/Resources/Partials/Footer.php")){
				require "Apps/$this->app/Resources/Partials/Footer.php";
			}
		}else{
			require "Apps/$this->app/Resources/Private/".$action.".php";
		}
	}

	function assign($var, $value){
		$this->{$var} = $value;
	}

	public function createLink($text, $attr = null, $action = 'index', $params = null, $controller=null, $app=null){
		if($app!=null){ $_SESSION['app'] = $app; }
		if($controller!=null){ $_SESSION['controller'] = $controller; }
		$link = "<a href = '".PATH."/".$action;
		if($params != null && is_array($params)){
			$link .= "?";
			foreach($params as $var => $val){
				$link .= "$var=".urlencode($val)."&";
			}
			$link = substr($link , 0 , (strlen($link) -1) );
			$link .= "' ";
		}
		if($attr!=null){
			if(is_array($attr)){
				foreach($attr as $a=>$b){
					$link .= "$a='$b' ";
				}
			}
		}
		$link .= ">$text</a>";
		return $link;
	}

	public function createPath($action, $params=null, $controller=null, $app=null){
		if($app!=null){ $_SESSION['app'] = $app; }
		if($controller!=null){ $_SESSION['controller'] = $controller; }
		$path = PATH."/$action/";
		if($params != null && is_array($params)){
			$path .= "?";
			foreach($params as $var => $val){
				$path .= "$var=".urlencode($val)."&";
			}
			$path = substr($path , 0 , (strlen($path) -1) );
			$path .= "'";
		}
		return $path;
	}

}

?>
