<?php
namespace Library;

use mysqli;
use Exception;

class Database{
	/*
	*
	* Mysql Connection Object
	*/
	private $conn;

	function __construct($host, $username, $password, $db){
		try {
			if($this->conn = new mysqli($host, $username, $password, $db)){

			}else{
				throw new Exception("Connection Error");
			}
		}
		catch(Exception $e){
			echo "<div class='alert alert-danger'>$e</div>";
		}
	}

	/*
	*
	* Mysql Custom Query like create and other
	* String $sql = query String
	*
	*/
	public function custom($sql){
		if($res = $this->conn->query($sql)){
			return $res;
		}else{
			return false;
		}
	}

	/*
	*
	* Mysql Insert Function
	* String $table = Name of Table
	* Array $fields = Associative Array as key = database field, value = value to be insert
	*
	* @return Object
	*/
	public function insert($table, $fields){
		$sql = sprintf("INSERT INTO $table (%s) VALUES ('%s')", implode(',', array_keys($fields)), implode("','", array_values($fields)));
		//echo $sql;die;
		try{
			if($this->conn->query($sql) == TRUE){
				return true;
			}else{
				throw new Exception($this->conn->error);
			}
		}catch(Exception $e){
			echo $e->getMessage();
		}
	}

	/*
	*
	* Mysql Update Function
	* String $table = Name of Table
	* Array $fields = Associative Array as key = database field which is to be update, value = value to be update
	* String $condition = Conditional Statement
	*
	* @return Object
	*/
	public function update($table, $fields, $condition){
		try{
			$sql = "update $table set";
	        foreach ($arr as $k => $v) {
	            $sql = $sql . " $k='$v',";
	        }
	        $sql = chop($sql, ",");
	        if ($condition != null) {
	            $sql = $sql . " where $condition";
	        }
	        if($this->conn->query($sql) == TRUE){
	        	return true;
	        }else{
	        	return false;
	        }
		}catch(Exception $e){
			echo $e->getMessage();
		}
	}

	/*
	*
	* Mysql Delete Function (This function will update 'Deleted field to be 1, Not physically delete any records')
	* String $table = Name of Table
	* String $condition = Conditional Statement
	*
	* @return Object
	*/
	public function delete($table, $condition){
		try{
			$sql = "delete from $table ";
	        if ($condition != null) {
	            $sql = $sql . " where $condition";
	        }

	        if($this->conn->query($sql) == TRUE){
	        	return true;
	        }else{
	        	return false;
	        }
		}catch(Exception $e){
			echo $e->getMessage();
		}
	}

	/*
	* Mysql select function
	* String $sql = Custom select Query
	*
	* @return Object
	*/
	public function select($sql){
		try{
			if($res = $this->conn->query($sql))	{
				if($res->num_rows > 0){
					while($row = $res->fetch_assoc()){
						$data[] = $row;
					}
				}else{
					return false;
				}
			}else{
				return false;
			}
		}
		catch(Exception $e){
			echo $e->getMessage();
		}
	}

	/*
	* Mysql mysqli_num_rows function
	* String $res = Resource Variable of any select query
	*
	* @return Integer
	*/

	public function num_rows(){
		try{
			if($res = $this->conn->query($sql))	{
				if($res->num_rows > 0){
					return $res->num_rows;
				}else{
					return false;
				}
			}else{
				return false;
			}
		}
		catch(Exception $e){
			echo $e->getMessage();
		}
	}

	/*
	* Mysql mysqli_insert_id function
	* String $res = Resource Variable of last inserted record
	*
	* @return Integer
	*/

	public function last_id(){
		return $conn->insert_id;
	}

	/*
	*
	* Mysql Prepared statement
	* String $sql = query
	*
	* @return Result
	*/

	public function preparedStatement($sql){
		try{
			$this->stmt = $conn->prepare($sql);
		}catch(Exception $e){
			echo $e->getMessage();
		}
	}

	public function bindParams($params){
		try{
			if(is_array($params)){
				$params = implode(",", $params);
				$this->stmt->bind_param($params);
			}
		}catch(Exception $e){
			echo $e->getMessage();
		}
	}

	public function execPrepared(){
		try{
			if($this->stmt->execute() == TRUE){
				return true;
			}else{
				return false;
			}
		}catch(Exception $e){
			echo $e->getMessage();
		}
	}

}

?>
