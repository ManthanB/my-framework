<?php
namespace Library;


class Router{

	function __construct(){
		$url = isset($_GET['url']) ? $_GET['url'] : '';
		$url = explode("/", rtrim($url, "/"));
		$app = isset($_SESSION['app']) ? $_SESSION['app'] : 'Backend';
		$controller = isset($_SESSION['controller']) ? $_SESSION['controller'] : 'index';
		$action = isset($url[0]) && $url[0]!='' ? $url[0] : 'index';
		$params = isset($_GET) ? $_GET : null;

		$this->render($action, null, $controller, $app);
	}

	public function render($action, $params = null, $controller = null, $app = null){
	//public function render($app, $controller='index', $action=null, $parameters = null){

		$_SESSION['controller'] = $controller!=null ? $controller : 'index' ;
		$_SESSION['app'] = $app!=null ? $app : 'index' ;
		require "Apps/".$_SESSION['app']."/Configuration/Config.php";
		//$this->page = $page;
		$controllerName = "Apps\\".$_SESSION['app']."\Controllers\\".$_SESSION['controller'];

		$controller = new $controllerName($app, $this->page);
		$controller->initView($this->page);
		//echo $action;die;
		if(in_array($action, $this->configApp['controllers'][$_SESSION['controller']])){
			if(!empty($parameters)){
				$controller->{$action."Action"}($parameters);
			}else{
				$controller->{$action."Action"}();
			}
		}else{
			$controller->notFoundAction();
		}
	}

}

?>
