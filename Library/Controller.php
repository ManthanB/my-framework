<?php
namespace Library;

class Controller{
	function __construct($app, $page){
		$this->app = $app;
		$this->page = $page;
	}

	function initView(){
		$this->view = new View($this->app, $this->page);
	}

	function initModel($name, $modal){
		if($modal!=null){
			$modelName = "Apps\\".$this->app."\Models\\".$modal;
			$this->{$name} = new $modelName();
		}
	}

	function notFoundAction(){
		echo "Action not registered";
	}


}


?>
