<?php
namespace Apps\Backend\Models;
use Exception;

class index extends \Library\Model{

	function __construct(){
		parent::__construct();
	}

	function submit(){
		if(isset($_POST['username'])){

		}
	}

	function checkSetup(){
		if($res = $this->Repo->custom("show tables")){
			if($res->num_rows==0){
				return false;
			}else{
				return true;
			}
		}
	}

	function setup(){
		if(isset($_POST['username'])){
			if($this->validate->validate($_POST['username'], ALPHA) && $this->validate->filter($_POST['password'])){
				extract($_POST);
				if($this->validate->filter($password) == $this->validate->filter($cpassword)){
					$user['username'] = $username;
					$user['password'] = password_hash($password, PASSWORD_DEFAULT);

					$this->makeSetup();
					$this->createUser($user);
					return true;
				}
			}
		}
	}

	function makeSetup(){
		try{
			if($num = $this->Repo->custom("show tables")){
				if($num->num_rows==0){
					$sql = "
						CREATE TABLE be_users(
							id int(11) NOT NULL auto_increment,
							username varchar(200),
							password TEXT,
							crdate int(11) unsigned DEFAULT '0' NOT NULL,
							deleted tinyint(4) unsigned DEFAULT '0' NOT NULL,
							hidden tinyint(4) unsigned DEFAULT '0' NOT NULL,

							PRIMARY KEY (id)
						)
					";
					if($this->Repo->custom($sql)){
						return true;
					}else{
						throw new Exception("Create Query cannot Execute");
					}
				}
			}
		}catch(Exception $e){
			echo "<div class='alert alert-danger'>$e</div>";
			die;
		}
	}

	function createUser($user){
		// echo "<pre>",print_r($user);die;
		try{
			if(!empty($user)){
				$user['crdate'] = time();
				$user['deleted'] = 0;
				$user['hidden'] = 0;
				// print_r($user);die;
				if($num = $this->Repo->custom("select * from be_users where hidden!=1 and deleted!=1")){
					if($num->num_rows==0){
						if($this->Repo->insert("be_users", $user)){
							return true;
						}else{
								throw new Exception("Cannot create user");
						}
					}else{
						return false;
					}
				}

			}
		}catch(Exception $e){
			echo "<div class='alert alert-danger'>$e</div>";
			die;
		}
	}


}

?>
