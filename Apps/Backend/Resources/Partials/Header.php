<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->

	<head>
	<title><?php echo $this->page['title']; ?></title>
	<?php
	if(is_array($this->page['meta'])){
		foreach($this->page['meta'] as $k=>$v){
			echo "<meta name='$k' content='$v' />\n";
		}
	}

	if(is_array($this->page['css'])){
		foreach($this->page['css'] as $css){
			echo "<link rel='stylesheet' href='$css' />"."\n";
		}
	}
	if(is_array($this->page['headerJs'])){
		foreach($this->page['headerJs'] as $js){
			echo "<script src='$js'></script>\n";
		}
	}
	?>


	</head>
	<body>