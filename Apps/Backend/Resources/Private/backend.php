<div class="container">
  <div class="row">
    <div class="col-md-12">
      <div class="page-header">
        <h2>Welcome to <?php echo $this->page['title']; ?></h2>
      </div>
    </div>
    <div class="col-md-3"></div>
    <div class="col-md-6">
      <div class="panel panel-primary">
        <div class="panel-heading">
          <h4>Setup Your System</h4>
        </div>
        <div class="panel-body">
          <?php
            if(isset($this->err)){
              echo "<div class='alert alert-danger'>";
              echo $this->err;
              echo "</div>";
            }
          ?>
          <form method="post" id="form" action="<?=$this->createPath('setupSystem',null,'index', 'Backend');?>">
            <div class="form-group">
              <label for="username">Username for Backend *:</label>
              <input type="text" id="username" class="form-control" placeholder="Enter Username" name="username" required/>

            </div>
            <div class="form-group">
              <label for="password">Password:</label>
              <input type="password" id="password" class="form-control" placeholder="Enter Password" name="password" required/>
            </div>
            <div class="form-group">
              <label for="cpassword">Confirm Password:</label>
              <input type="password" id="cpassword" class="form-control" placeholder="Confirm Password" name="cpassword" required/>
            </div>
            <div class="form-group">
              <span class="hidden" style="color:#f00" id="err">Password and Confirm Password Does not match.</span>
            </div>
            <button type="submit" class="btn btn-block btn-primary">
              Sign Up <big><i class="fa fa-sign-in"></i></big>
            </button>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
