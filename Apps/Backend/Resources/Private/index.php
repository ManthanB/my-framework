<?php
    //echo $this->createLink("Click Me", array("class"=>"btn btn-danger"), "click", array("id"=>1, "msg"=>"Hello World"), "index", "Backend");
?>

<div class="container">
    <div class="page-header">
        <h1>This is Index Page</h1>
    </div>
    <div class="panel panel-danger">
      <div class="panel-heading">
        <h2>Panel Header</h2>
      </div>
    	<div class='panel-body'>
    		<form action="submit" method="post">
		    	<input type="text" class="form-control form-group" name="username"/>
		    	<input type="password" class="form-control form-group" name="password"/>
		    	<input type="submit" class="btn btn-danger" />
	    	</form>
    	</div>
    </div>
</div>
