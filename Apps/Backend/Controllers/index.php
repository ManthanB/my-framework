<?php
namespace Apps\Backend\Controllers;
// use Exception;
class index extends \Library\Controller{

	private $model;

	function __construct($app, $page){
		parent::__construct($app, $page);
		$this->initModel("indexModel", "index");
	}

	function indexAction(){
		if(!$this->indexModel->checkSetup()){
				$this->view->render("backend");
		}else{
				$this->view->render("index");
		}

	}

	function errorAction(){
		echo "404 Error";
	}

	function submitAction(){
		$data = $this->indexModel->submit();
		$this->view->render("index");
	}

	function setupSystemAction(){
		if($this->indexModel->setup()){
				$this->view->render("index");
		}else{
				$this->view->assign("err", "User Already Exists.");
				$this->view->render("backend");
		}
	}

}

?>
