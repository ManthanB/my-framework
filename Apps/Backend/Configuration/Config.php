<?php
	define("RES_PUBLIC", PATH . "/Apps/$_SESSION[app]/Resources/Public/");

	$this->configApp = array(
		'controllers' => array(
			'index' => array(
				'index', 'error', 'submit', 'setupSystem'
			),
		),

		'models' => array(
			'index',
		),
	);

	$this->page = array(
		'title' => 'My Own MVC',
		'meta' => array(
			'viewport' => 'width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no',
			'author' => 'Manthan Budheliya'
		),
		'css' => array(
			1 => 'http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700',
			3 => RES_PUBLIC."plugins/bootstrap/css/bootstrap.min.css",
			4 => RES_PUBLIC."plugins/font-awesome/css/font-awesome.min.css",
			10 => RES_PUBLIC."plugins/bootstrap-datepicker/css/datepicker.css",
			11 => RES_PUBLIC."plugins/bootstrap-datepicker/css/datepicker3.css",
		),

		'headerJs' => array(
		),

		'footerJs' => array(
			1 => RES_PUBLIC."plugins/jquery/jquery-1.9.1.min.js",
			3 => RES_PUBLIC."plugins/jquery-ui/ui/minified/jquery-ui.min.js",
			4 => RES_PUBLIC."plugins/bootstrap/js/bootstrap.min.js",
			5 => RES_PUBLIC."plugins/bootstrap-datepicker/js/bootstrap-datepicker.js",
			6 => RES_PUBLIC."js/custom.js",
		),

		'footerData' => "
			<!--[if lt IE 9]>
					<script src='assets/crossbrowserjs/html5shiv.js'></script>
					<script src='assets/crossbrowserjs/respond.min.js'></script>
					<script src='assets/crossbrowserjs/excanvas.min.js'></script>
				<![endif]-->

			<script>
			$(document).ready(function() {
				App.init();
				Dashboard.init();
			});
		</script>",
	);



?>
